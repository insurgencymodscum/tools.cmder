@rem ####################################################################
@rem IMS: cmdercfg-path-add-msysgit-to-path.bat
@rem add msysgit (git) to path
@rem ####################################################################

@echo off

echo [%~nx0]: Running "%~dpnx0"

set msysgit01=%CMDER_ROOT%\vendor\msysgit
set msysgit02=%msysgit01%\bin
set msysgit03=%msysgit01%\mingw32\bin
set msysgit04=%msysgit01%\cmd
set msysgit05=%msysgit01%\usr\bin

set PATH=%PATH%;%msysgit01%;%msysgit02%;%msysgit03%;%msysgit04%;%msysgit05%;

echo [%~nx0]: ADDED MSYSGIT TO PATH!