@rem ####################################################################
@rem IMS: cmdercfg-path-add-python3-to-path.bat
@rem add python 3.x to path
@rem ####################################################################

@echo off

echo [%~nx0]: Running "%~dpnx0"

set WINPYDIR=%tools%\dev\WinPython64-3.6.1.0-Zero\python-3.6.1.amd64

rem from WinPython64-3.6.1.0-Zero\scripts\env.bat:
PATH=%WINPYDIR%\Lib\site-packages\PyQt5;%WINPYDIR%\Lib\site-packages\PyQt4;%WINPYDIR%\;%WINPYDIR%\DLLs;%WINPYDIR%\Scripts;%WINPYDIR%\..\tools;%WINPYDIR%\..\tools\mingw32\bin;%WINPYDIR%\..\tools\R\bin\x64;%WINPYDIR%\..\tools\Julia\bin;%PATH%;

echo [%~nx0]: ADDED PYTHON 3.X TO PATH!