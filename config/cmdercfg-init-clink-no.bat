@rem ####################################################################
@rem IMS: cmder\config\cmdercfg-init-clink-yes.bat
@rem start main init batch WITH or WITHOUT clink
@rem ####################################################################

@echo off

echo [%~nx0]: Running "%~dpnx0"

call %CMDER_ROOT%\config\cmdercfg-init-for-cmd.exe.bat "clinkno"