@rem ###########################################################
@rem IMS: cmder\config\cmdercfg-path.bat
@rem adding folders to path
@rem also called when starting TotalCmd
@rem ###########################################################

@echo off

echo [%~nx0]: Running "%~dpnx0"

rem used for "cd" aliases
set HOMEDIR=d:\tmp

rem ============================================================

rem moved from cmdercfg-init-for-cmd.exe.bat

rem softcoded:
rem rem Find root dir
rem @if not defined CMDER_ROOT (
rem     for /f %%i in ("%ConEmuDir%\..\..") do @set CMDER_ROOT=%%~fi
rem )
rem hardcoded for totalcmd:
set CMDER_ROOT=D:\tools\cmder

rem ============================================================

REM %ConEmuWorkDir% is used by "create new console, startup directory for new process"
rem %CMDER_START% is used to start cmder in the given dir

rem make sure these work!:
rem start from ~runCmderEXELauncher_CDR_CMDR.bat
rem in conemu: start new task in the given dir
rem in totalcmd: start cmder in the given dir via tc\.wip\run.cmder.here.bat

rem ============================================================

rem need %tools% defined for py2 and py3 path scripts
set tools=%CMDER_ROOT%\..\..\tools
set tools2=%CMDER_ROOT%\..\..\tools2

set path01=%tools%
set path02=%tools2%

set path03=%CMDER_ROOT%\config

set path04=%LOCALAPPDATA%\Spoon\Cmd
set path05=%ProgramFiles%\Oracle\VirtualBox

rem set path06=%tools%\util\7-ZipPortable\App\7-Zip64
rem set path07=%tools%\util\7-ZipPortable\App\7-Zip

rem totalcmd and double commander
set path08=%tools%\tc\.wip
set path09=%tools%\dc\.wip

rem autohotkey
set path10=%tools%\AHK

set path11=%tools%\dev\jre7u131x86\bin
rem node's npm.cmd
set path12=%tools%\dev\node-v6.4.0-win-x64

rem audio-video
set path13=%tools%\AV\VIDEO\mkvtoolnix-64bit-9.2.0
set path14=%tools%\av\video\FFMPEG
set path15=%tools%\av\audio\sox-14.4.2

set path16=%tools%\NirSoft\NirSoft\x64
set path17=%tools%\NirSoft\NirSoft
set path18=%tools%\sys\SysInternalsSuite

rem adb.exe for android
set path19=%tools%\dev\platform-tools_r28.0.0-windows

rem network testing
set path20=%tools%\util.net\httping-2.5-win64
set path21=%tools%\util.net\bwping-1.9-win32
set path22=%tools%\util.net\hrping-v507

rem flac.exe
set path23=%tools%\av\audio\flac-1.3.2-win\win64

set path24=%programfiles(x86)%\VMware\VMware Workstation
set path25=%tools%\net\BIND9.11.4-P2.x86

rem improvised shortcut manager
set path26=%tools%\sys\Splat

set path27=%tools%\dev\Ruby\bin

rem ============================================================

rem default paths
set PATH=%PATH%;%CMDER_ROOT%\bin;%CMDER_ROOT%;

rem custom paths
set PATH=%PATH%;%path01%;%path02%;%path03%;%path04%;%path05%;
set PATH=%PATH%;%path06%;%path07%;%path08%;%path09%;%path10%;
set PATH=%PATH%;%path11%;%path12%;%path13%;%path14%;%path15%;
set PATH=%PATH%;%path16%;%path17%;%path18%;%path19%;%path20%;
set PATH=%PATH%;%path21%;%path22%;%path23%;%path24%;%path25%;
set path=%PATH%;%path26%;%path27%;
rem ============================================================