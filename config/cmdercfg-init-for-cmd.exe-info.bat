@rem ####################################################################
@rem IMS: cmder\config\cmdercfg-init-for-cmd.exe-info.bat
@rem shows reminders, notes, called by cmdercfg-init-for-cmd.exe.bat
@rem ####################################################################

@echo off

echo [%~nx0]: Running "%~dpnx0"

rem ###########################################################################

rem ver
rem echo [%~nx0]: DEBUG: ComSpec:"%ComSpec%"
rem echo [%~nx0]: DEBUG: CMDER_ROOT:"%CMDER_ROOT%"
rem echo [%~nx0]: DEBUG: HOME:"%HOME%"
rem echo [%~nx0]: DEBUG: CMDER_START:"%CMDER_START%"
rem echo [%~nx0]: DEBUG: ConEmuWorkDir:"%ConEmuWorkDir%"
rem echo [%~nx0]: DEBUG: %%CD%%:"%CD%"
rem pause

rem ###########################################################################

echo [%~nx0]: NOTE: Add Python 2.x/3.x to path (using aliases): "py2" or "py3".
echo [%~nx0]: NOTE: Add MSYSGIT to path (using aliases): "msysgit".

rem ###########################################################################