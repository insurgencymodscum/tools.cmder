@rem ####################################################################
@rem IMS: cmder\config\cmdercfg-path-add-python2-to-path.bat
@rem add python 2.x to path
@rem ####################################################################

@echo off

echo [%~nx0]: Running "%~dpnx0"

set path01=%tools%\dev\WinPython64-2.7.13\python-2.7.13.amd64
set path02=%tools%\dev\WinPython64-2.7.13\python-2.7.13.amd64\Scripts

set PATH=%PATH%;%path01%;%path02%;

echo [%~nx0]: ADDED PYTHON 2.7 TO PATH!