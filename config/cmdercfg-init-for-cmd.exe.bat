@rem ####################################################################
@rem IMS: cmder\config\cmercfg-init-for-cmd.exe.bat
@rem config for cmd.exe, used in cmder's "tasks"
@rem need admin rights for mklink.exe (see below)!
@rem ####################################################################

@echo off

echo [%~nx0]: Running "%~dpnx0"

rem ###########################################################################

rem prompt style

rem http://ascii-table.com/ansi-escape-sequences.php
rem https://kb.iu.edu/d/aamm
rem https://msdn.microsoft.com/en-us/library/cc722862.aspx

rem ADJUST {git} COLOUR IN cmder\config\git.lua !

rem $e[x;xx;xxm is the color, $d date, $t time, $p path, $$ dollarsign, $e[0m turns off all colors

rem original: @prompt $E[1;32;40m$P$S{git}$S$_$E[1;30;40m{lamb}$S$E[0m
rem with date and time: @prompt $e[1;33;40m $d $e[1;34;40m $t $e[1;32;40m $p $$

rem current prompt (readable): @prompt $e[1;32;40m $p $e[0m {git} $e[0m $e[1;31;48m $$ $e[0m

set arg1=%~1

if "%arg1%" == "clinkyes" (
rem with current git branch
@prompt $e[1;32;40m$p$e[0m{git}$e[0m$e[1;31;48m$$$e[0m
)

if "%arg1%" == "clinkno" (
rem without current git branch
@prompt $e[1;32;40m$p$e[0m$e[0m$e[1;31;48m$$$e[0m
)

rem ###########################################################################

rem run clink

rem add to conemu tasks:
rem set arch=64 & "%windir%\system32\cmd.exe"...
@if "%arch%"=="32" (set architecture=86) else (set architecture=64)

if "%arg1%" == "clinkyes" (
  echo [%~nx0]: Starting Clink x%arch%!
  rem FINICKY! DO NOT TOUCH! must have "--quiet" and NOT "--nolog" at specific place to work!
  rem possibly: "--nolog" is broken with clink 0.4.9 (worked with 0.4.8)
  "%CMDER_ROOT%\vendor\clink\clink_x%architecture%.exe" inject --profile "%CMDER_ROOT%\config\clink" --scripts "%CMDER_ROOT%\config\clink" --quiet"
)

if "%arg1%" == "clinkno" (
  echo [%~nx0]: NOT starting Clink %arch%!
)

rem ###########################################################################

rem Add default cmder and custom path
call %CMDER_ROOT%\config\cmdercfg-path.bat

rem ###########################################################################

echo [%~nx0]: Starting DOSKEY aliases.
doskey /macrofile="%CMDER_ROOT%\config\cmdercfg-doskey-aliases.txt"

rem ###########################################################################

rem using vimrc in config dir instead of in msysgit dir
rem NEED ADMIN RIGHTS FOR MKLINK!
rem ---
rem Check if file exists: one-liner
if not exist "%CMDER_ROOT%\config\vimrc" (
  echo [%~nx0]: Making symbolic link for vimrc.
  rem del %CMDER_ROOT%\config\msysgit\etc\vimrc
  mklink %CMDER_ROOT%\vendor\msysgit\etc\vimrc %CMDER_ROOT%\config\vimrc
)

rem ###########################################################################

rem show reminders, notes, etc.
call %CMDER_ROOT%\config\cmdercfg-init-for-cmd.exe-info.bat

rem ###########################################################################