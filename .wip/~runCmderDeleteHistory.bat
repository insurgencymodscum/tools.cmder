@echo off

rem Set the current directory to the location of the batch script, using the %0 parameter
pushd "%~dp0"

rem /f Force deleting of read-only files.
rem /q Quiet mode, do not ask if ok to delete a global wildcard.
del /f /q ..\config\.history
del /f /q ..\vendor\clink\profile\.history
del /f /q ..\vendor\conemu-maximus5\ConEmu.xml

del %USERPROFILE%\.viminfo

rem Change path back to the the most recent path set by the PUSHD command.
popd