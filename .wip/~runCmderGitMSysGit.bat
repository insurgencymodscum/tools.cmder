@echo off

rem rem Begin localisation of Environment Variables.
rem setlocal

rem Set the current directory to the location of the batch script, using the %0 parameter
pushd "%~dp0"

rem note %CMDER_ROOT% needs to be explicitly defined in "cmder\config\cmdercfg-path.bat"
call d:\tools\cmder\config\cmdercfg-path.bat

rem adding msysgit to path
call ..\config\cmdercfg-path-add-msysgit-to-path.bat

rem Start an application without creating a new window.
start /b ..\cmder.exe

rem Change path back to the the most recent path set by the PUSHD command.
popd

rem rem Restore any environment variables present before the SETLOCAL was issued.
rem endlocal