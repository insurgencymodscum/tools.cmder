@rem based on code.scripts.batch\code.snippets.examples.templates.batch\code.bat.run.template.here.bat

@echo off

rem Begin localisation of Environment Variables.
setlocal

rem Set the current directory to the location of the batch script, using the %0 parameter
pushd "%~dp0"

rem Change directory back to the path/folder most recently stored by the PUSHD command.
popd

rem note %CMDER_ROOT% needs to be explicitly defined in "cmder\config\cmdercfg-path.bat"
call d:\tools\cmder\config\cmdercfg-path.bat

rem adding msysgit to path
call ..\config\cmdercfg-path-add-msysgit-to-path.bat

rem NEED %CD% HERE!
rem need quotes around %* for folders with spaces
start /b %cd%\..\cmder.exe /start "%*"

rem Restore any environment variables present before the SETLOCAL was issued.
endlocal

exit /b