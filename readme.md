# Versions #

   See: cmder\.wip\backup.installs\ 

# About #

   Cmder is a distribution of ConEmu (Windows console emulator), Clink, clink-completions, & MSysGit (AKA PortableGit).

   Note: "autocrlf=false" in ".git\config"!

   ~~~
   ConEmu changelog:
   https://conemu.github.io/en/Whats_New.html

   ConEmu download:
   https://github.com/Maximus5/ConEmu/tags
   ~~~

# Components #

   ~~~
   cmder.exe from Cmder
   clink-completions
   Clink
   ConEmu
   PortableGit (AKA MSysGit AKA "Git For Windows")
   ~~~

   ## Needed by "cmder114xplauncher.exe" ##

      ~~~
      msvcp120.dll
      msvcr120.dll
      ~~~

# Updating PortableGit (AKA MSysGit) #

   **Update: Unnecessary(?)** 

   ~~Don't forget to run `post-install.bat`!:~~

   ~~~
   cmd.exe
   cd PortableGit
   git-bash.exe --no-needs-console --hide --no-cd --command=post-install.bat
   ~~~